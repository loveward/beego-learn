package main

import (
	_ "beego-learn/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

